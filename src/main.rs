use clap::{crate_authors, crate_description, crate_version, App, Arg};
use cli_textsearch::get_regex_search_results;
use cli_textsearch::search_structs::{FileSearchParams, SearchParams};

fn main() {
    const PATH: &'static str = "path";
    const CONTAINS: &'static str = "contains";
    const STARTS_WITH: &'static str = "starts-with";
    const ENDS_WITH: &'static str = "ends-with";
    const CONTAINS_DIGITS: &'static str = "Contains-Digits";
    const CONTAINS_A_Z_LOWER_CASE: &'static str = "Contains-chars-[a-z]";
    const CONTAINS_A_Z_UPPER_CASE: &'static str = "Contains-chars-[A-Z]";
    const SEARCH_RECURSIVELY: &'static str = "Search-recursively";
    const FILE_EXTENSIONS: &'static str = "file-extensions";

    let matches = App::new("CLI Text Search")
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name(PATH)
                .short("p")
                .long(PATH)
                .required(true)
                .multiple(true)
                .number_of_values(1)
                .value_name("PATH")
                .help(
                    "Path to file OR DIR to search within.
                        Can be used multiple times 
                        example -p c:/file1.txt -p c:/file2.txt.",
                )
                .takes_value(true),
        )
        .arg(
            Arg::with_name(SEARCH_RECURSIVELY)
                .short("r")
                .required(false)
                .takes_value(false)
                //.index(4)
                .help("Should search be for multiple files recursively"),
        )
        .arg(
            Arg::with_name(FILE_EXTENSIONS)
                .long("ext")
                .help("File extensions to search for. Examples: --ext json txt")
                .min_values(1),
        )
        .arg(
            Arg::with_name(CONTAINS)
                .short("c")
                .long(CONTAINS)
                .help("Text that contain the character or string")
                .required(false)
                .takes_value(true),
        )
        .arg(
            Arg::with_name(STARTS_WITH)
                .short("s")
                .long(STARTS_WITH)
                .help("character or string that the text start with.")
                .required(false)
                .takes_value(true)
                .help("Character or string that the text starts with."),
        )
        .arg(
            Arg::with_name(ENDS_WITH)
                .short("e")
                .long(ENDS_WITH)
                .required(false)
                .takes_value(true)
                //.index(4)
                .help("Character or string that the text ends with."),
        )
        .arg(
            Arg::with_name(CONTAINS_DIGITS)
                .short("d")
                .required(false)
                .takes_value(false)
                //.index(4)
                .help("Does the text contain digits"),
        )
        .arg(
            Arg::with_name(CONTAINS_A_Z_LOWER_CASE)
                .short("a")
                .required(false)
                .takes_value(false)
                //.index(4)
                .help("Does the text contain characters [a-z] (case sensitive)"),
        )
        .arg(
            Arg::with_name(CONTAINS_A_Z_UPPER_CASE)
                .short("A")
                .required(false)
                .takes_value(false)
                //.index(4)
                .help("Does the text contain characters [A-Z] (case sensitive)"),
        )
        .get_matches();

    // Gets a value for path if supplied by user
    let paths: Vec<String> = matches
        .values_of(PATH)
        .unwrap()
        .map(|file| file.to_string())
        .collect();
    // Gets a value for CONTAINS if supplied by user
    let contains = matches.value_of(CONTAINS).unwrap_or("").to_string();
    let starts_with = matches.value_of(STARTS_WITH).unwrap_or("").to_string();
    let ends_with = matches.value_of(ENDS_WITH).unwrap_or("").to_string();
    let contains_digits = matches.is_present(CONTAINS_DIGITS);
    let contains_a_z = matches.is_present(CONTAINS_A_Z_LOWER_CASE);
    let contains_A_Z = matches.is_present(CONTAINS_A_Z_UPPER_CASE);
    let search_recursively = matches.is_present(SEARCH_RECURSIVELY);
    let file_extensions = if matches.is_present(FILE_EXTENSIONS) {
        let str_vec: Vec<&str> = matches.values_of(FILE_EXTENSIONS).unwrap().collect();
        let result: Option<Vec<String>> =
            Option::Some(str_vec.iter().map(|s| s.to_string()).collect());
        result
    } else {
        Option::None
    };

    //ln!("Ext: {:?}", file_extensions);
    let file_search_params = FileSearchParams::new(paths, search_recursively, file_extensions);

    let search_parames = SearchParams::new(
        file_search_params,
        contains,
        starts_with,
        ends_with,
        contains_digits,
        contains_a_z,
        contains_A_Z,
    );

    let results = get_regex_search_results(&search_parames);
    println!("Search parameters: {}", &search_parames);
    println!("Search results:");
    results.iter().for_each(|(k, v)| {
        print!("File: {:?} ", k);
        println!("detected for the following results: {:?}", v);
    })
}
