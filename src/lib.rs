use crate::file_handler::{get_file_content, get_files_by_extensions};
use crate::search_structs::SearchParams;
use regex::RegexSet;
use std::collections::HashMap;

/// Function to return results of the search or empty array if none found
pub fn get_regex_search_results(search_params: &SearchParams) -> HashMap<String, Vec<String>> {
    return get_multi_file_results(&search_params);
}

/// Function to get results from a single file
/// Results will be in a hashmap with the full file path as key and the regex values used to get that file as a result
fn get_single_file_results(search_params: &SearchParams) -> HashMap<String, Vec<String>> {
    let mut results: HashMap<String, Vec<String>> = HashMap::new();
    let file_path = search_params.file_search_params.path.clone();
    get_regex_results_for_files(&search_params, file_path, &mut results);
    return results;
}

/// Function to get results from multiple files
/// Results will be in a hashmap with the full file path as key and the regex values used to get that file as a result
/// If file list is empty, will return an empty Hashmap
fn get_multi_file_results(search_params: &SearchParams) -> HashMap<String, Vec<String>> {
    let file_list = get_files_by_extensions(
        &search_params.file_search_params.path,
        &search_params.file_search_params.file_extensions,
        search_params.file_search_params.is_search_recursive,
    );
    let mut results: HashMap<String, Vec<String>> = HashMap::new();
    get_regex_results_for_files(search_params, file_list, &mut results);
    return results;
}

/// Function to get regex results for a file
/// If results found they will be added to the hashmap passed in
fn get_regex_results_for_files(
    search_params: &SearchParams,
    file_paths: Vec<String>,
    results: &mut HashMap<String, Vec<String>>,
) {
    validate_file_paths(&file_paths);
    file_paths.iter().for_each(|file_path| {
        let reg_strings = get_regex_strings(&search_params);
        let text = get_text_from_file(file_path);
        if text.is_some() {
            let str_results = get_regex_matches(&text.unwrap().as_str(), reg_strings);
            //Only insert results with values
            if str_results.len() > 0 {
                results.insert(file_path.clone(), str_results);
            }
        }
    })
}
/// check each file path to make sure it is not a DIR or has errors
fn validate_file_paths(file_paths: &Vec<String>) {
    use std::fs;
    file_paths.iter().for_each(|file_path| {
        let metadate = fs::metadata(&file_path);
        if metadate.is_err() || metadate.unwrap().is_dir() {
            panic!("File with path: {} could not be opened. Verify it is a valid file path");
        }
    });
}

/// Function to get text from a file
/// Searches for the file, reads it's content and return is found
/// Will return Option::None if file doesn't contain text
/// * `path` - the full path to the file. Example: "c:/tmp/text.txt"
///
fn get_text_from_file(path: &String) -> Option<String> {
    if path.is_empty() {
        None
    } else {
        Some(get_file_content(path))
    }
}

fn get_regex_set(regex_strings: &Vec<String>) -> RegexSet {
    let regex_set = RegexSet::new(regex_strings.into_iter());
    match regex_set {
        Ok(regex_set) => regex_set,
        Err(error) => panic!("Error creating regexSet: {}", error),
    }
}

/// Get a Vec of String with regex according to the search params passed in
fn get_regex_strings(search_params: &SearchParams) -> Vec<String> {
    let mut regex = Vec::new();
    if !search_params.contains.is_empty() {
        regex.push(String::from(&search_params.contains));
    }
    if search_params.contains_digits {
        regex.push(String::from("\\d"));
    }
    if search_params.contains_A_Z {
        regex.push(String::from("[A-Z]"));
    }
    if search_params.contains_a_z {
        regex.push(String::from("[a-z]"));
    }
    if !search_params.starts_with.is_empty() {
        let starts_with = String::from(format!("^{}", search_params.starts_with));
        regex.push(starts_with);
    }
    if !search_params.ends_with.is_empty() {
        let ends_with = String::from(format!("{}$", search_params.ends_with));
        regex.push(ends_with);
    }
    regex
}

fn get_regex_matches(text_to_search: &str, reg_values: Vec<String>) -> Vec<String> {
    let mut results: Vec<String> = Vec::new();
    let set = get_regex_set(&reg_values);
    let matches = set.matches(text_to_search);
    //if not matches return empty Vec
    if !matches.matched_any() {
        return results;
    }

    matches
        .iter()
        .filter(|reg| reg_values.get(*reg as usize).is_some())
        .for_each(|reg| results.push(reg_values.get(reg as usize).unwrap().to_owned()));

    results
}

pub mod search_structs {
    use std::fmt;

    #[derive(Debug)]
    pub struct FileSearchParams {
        pub path: Vec<String>,
        pub is_search_recursive: bool,
        pub file_extensions: Option<Vec<String>>,
    }

    impl FileSearchParams {
        pub fn new(
            path: Vec<String>,
            is_search_recursively: bool,
            file_extensions: Option<Vec<String>>,
        ) -> Self {
            FileSearchParams {
                path,
                is_search_recursive: is_search_recursively,
                file_extensions,
            }
        }
    }

    impl fmt::Display for FileSearchParams {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(
                f,
                "path: {:?}, search recursively: {}, file extensions: {:?}",
                self.path, self.is_search_recursive, self.file_extensions
            )
        }
    }

    #[derive(Debug)]
    pub struct SearchParams {
        pub file_search_params: FileSearchParams,
        pub contains: String,
        pub starts_with: String,
        pub ends_with: String,
        pub contains_digits: bool,
        pub contains_a_z: bool,
        pub contains_A_Z: bool,
    }

    impl SearchParams {
        pub fn new(
            path: FileSearchParams,
            contains: String,
            starts_with: String,
            ends_with: String,
            contains_digits: bool,
            contains_a_z: bool,
            contains_A_Z: bool,
        ) -> Self {
            SearchParams {
                file_search_params: path,
                contains,
                starts_with,
                ends_with,
                contains_digits,
                contains_a_z,
                contains_A_Z,
            }
        }
    }

    impl fmt::Display for SearchParams {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(
                f,
                "\nFile search params: {},\n contains: {},\n starts with: {},\n ends with: {},\n contains digits: {},\n contains: a-z {},\n contains A-Z: {}",
                self.file_search_params, self.contains, self.starts_with, self.ends_with, self.contains_digits, self.contains_a_z,self.contains_A_Z
            )
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::search_structs::{FileSearchParams, SearchParams};
    use crate::{
        get_multi_file_results, get_regex_matches, get_regex_set, get_regex_strings,
        get_single_file_results,
    };
    use std::collections::HashMap;

    #[test]
    fn get_single_file_results_test() {
        let test_file_search_param = FileSearchParams::new(
            vec![String::from("./tests/resources/test_text_parent_dir.txt")],
            false,
            Option::None,
        );

        let test_search_params = SearchParams::new(
            test_file_search_param,
            "fooBar".to_string(),
            "foo".to_string(),
            "bar".to_string(),
            true,
            true,
            true,
        );
        let result = get_single_file_results(&test_search_params);

        let mut expected = HashMap::new();
        expected.insert(
            String::from("./tests/resources/test_text_parent_dir.txt"),
            vec![
                String::from("\\d"),
                String::from("[A-Z]"),
                String::from("[a-z]"),
                String::from("bar$"),
            ],
        );
        assert_eq!(1, result.len());
        assert_eq!(expected, result);
    }

    #[test]
    fn get_multi_file_results_test_one_path() {
        let test_file_search_param = FileSearchParams::new(
            vec![String::from("./tests/resources")],
            true,
            Option::Some(vec![".json".to_string(), ".txt".to_string()]),
        );

        let test_search_params = SearchParams::new(
            test_file_search_param,
            "fooBar".to_string(),
            "foo".to_string(),
            "bar".to_string(),
            true,
            true,
            true,
        );
        let results = get_multi_file_results(&test_search_params);
        assert_eq!(5, results.len());
        let mut expected = HashMap::new();
        expected.insert(
            String::from("./tests/resources\\sub-folder1\\test-text1.json"),
            vec![String::from("\\d"), String::from("[a-z]")],
        );
        expected.insert(
            String::from("./tests/resources\\sub-folder1\\sub-sub-folder1\\text-file-nested1.txt"),
            vec![
                String::from("\\d"),
                String::from("[A-Z]"),
                String::from("[a-z]"),
            ],
        );
        expected.insert(
            String::from("./tests/resources\\sub-folder2\\test-text2.json"),
            vec![String::from("\\d"), String::from("[a-z]")],
        );
        expected.insert(
            String::from("./tests/resources\\sub-folder2\\sub-sub-folder2\\text-file-nested2.txt"),
            vec![
                String::from("\\d"),
                String::from("[A-Z]"),
                String::from("[a-z]"),
            ],
        );
        expected.insert(
            String::from("./tests/resources\\test_text_parent_dir.txt"),
            vec![
                String::from("\\d"),
                String::from("[A-Z]"),
                String::from("[a-z]"),
                String::from("bar$"),
            ],
        );
        //compare hashmap results
        for (key, value) in expected {
            assert!(results.contains_key(key.as_str()));
            println!("testing key: {}", key);
            assert_eq!(&value, results.get(key.as_str()).unwrap());
        }
    }

    #[test]
    fn get_multi_file_results_test_two_paths() {
        let test_file_search_param = FileSearchParams::new(
            vec![
                String::from(".\\tests\\resources\\sub-folder1"),
                String::from(".\\tests\\resources\\sub-folder2"),
            ],
            true,
            Option::Some(vec![".json".to_string(), ".txt".to_string()]),
        );

        let test_search_params = SearchParams::new(
            test_file_search_param,
            "fooBar".to_string(),
            "foo".to_string(),
            "bar".to_string(),
            true,
            true,
            true,
        );
        let results = get_multi_file_results(&test_search_params);

        let mut expected = HashMap::new();
        get_test_hashmap_paths_two_paths(&mut expected);
        println!("expected: {:?}", expected);
        println!("Results: {:?}", results);
        //compare hashmap results
        assert_eq!(expected.len(), results.len());
        for (key, value) in expected {
            println!("testing Key: {}", key);
            assert!(results.contains_key(key.as_str()));
            println!("testing key: {}", key);
            assert_eq!(&value, results.get(key.as_str()).unwrap());
        }
    }

    fn get_test_hashmap_paths_two_paths(expected: &mut HashMap<String, Vec<String>>) {
        let text_file_nested2 = String::from(
            ".\\tests\\resources\\sub-folder2\\sub-sub-folder2\\text-file-nested2.txt",
        );
        let test_text1 = String::from(".\\tests\\resources\\sub-folder1\\test-text1.json");
        let test_text2 = String::from(".\\tests\\resources\\sub-folder2\\test-text2.json");
        let text_file_nested1 = String::from(
            ".\\tests\\resources\\sub-folder1\\sub-sub-folder1\\text-file-nested1.txt",
        );
        expected.insert(
            text_file_nested2,
            vec![
                String::from("\\d"),
                String::from("[A-Z]"),
                String::from("[a-z]"),
            ],
        );
        expected.insert(test_text2, vec![String::from("\\d"), String::from("[a-z]")]);
        expected.insert(
            text_file_nested1,
            vec![
                String::from("\\d"),
                String::from("[A-Z]"),
                String::from("[a-z]"),
            ],
        );
        expected.insert(test_text1, vec![String::from("\\d"), String::from("[a-z]")]);
    }

    #[test]
    fn get_regex_matches_test() {
        let test_vec = vec![
            "fooBar".to_string(),
            "\\d".to_string(),
            "[A-Z]".to_string(),
            "[a-z]".to_string(),
            "^foo".to_string(),
            "bar$".to_string(),
        ];
        let text = "this is a test for testing regex matches 12 and bar";
        let result = get_regex_matches(text, test_vec);
        let expected = vec!["\\d".to_string(), "[a-z]".to_string(), "bar$".to_string()];
        assert_eq!(expected.len(), result.len());
        assert_eq!(expected, result);
    }

    #[test]
    fn get_regex_set_test() {
        let test_vec = vec![
            "fooBar".to_string(),
            "\\d".to_string(),
            "[A-Z]".to_string(),
            "[a-z]".to_string(),
            "^foo".to_string(),
            "bar$".to_string(),
        ];
        let result = get_regex_set(&test_vec);
        assert_eq!(6, result.len());
        assert_eq!("fooBar", result.patterns().iter().nth(0).unwrap());
        assert_eq!("\\d", result.patterns().iter().nth(1).unwrap());
        assert_eq!("bar$", result.patterns().iter().last().unwrap());
    }

    #[test]
    fn get_regex_string_test() {
        let test_file_search_param = FileSearchParams::new(
            vec![String::from(".")],
            true,
            Option::Some(vec![".json".to_string(), ".txt".to_string()]),
        );

        let test_search_params = SearchParams::new(
            test_file_search_param,
            "fooBar".to_string(),
            "foo".to_string(),
            "bar".to_string(),
            true,
            true,
            true,
        );

        let results = get_regex_strings(&test_search_params);
        assert_eq!(6, results.len());
        assert_eq!(
            vec![
                "fooBar".to_string(),
                "\\d".to_string(),
                "[A-Z]".to_string(),
                "[a-z]".to_string(),
                "^foo".to_string(),
                "bar$".to_string(),
            ],
            results
        );
    }
}

pub mod file_handler {
    /// This function get the text from a file
    /// # Panics
    ///
    /// The function panics if the file can't be found or opened.
    ///
    /// ```rust,should_panic
    /// // panics on file that doesn't exist
    /// cli_textsearch::file_handler::get_file_content(&String::from("file.txt"));
    /// ```
    pub fn get_file_content(path: &String) -> String {
        use std::fs;
        let contents = fs::read_to_string(path);
        match contents {
            Err(error) => panic!("Problem opening the file: {} Error: {:?}", &path, error),
            Ok(contents) => contents,
        }
    }
    /// Returns a list of files (with full path) that have a specific extension
    /// The search can be recursive from the folder given if true passed into the method
    ///
    /// # Arguments
    ///
    /// * `rootPath` - A string slice that holds the name of the root folder to start the search in
    /// * `file_extensions` - A Vec of string slice that holds the names of the file extension to search for
    /// * `is_recursive` - A bool that represents if search should be recursive. A 'true' value will recursively.
    ///
    pub fn get_files_by_extensions(
        root_paths: &Vec<String>,
        file_extensions: &Option<Vec<String>>,
        is_recursive: bool,
    ) -> Vec<String> {
        use walkdir::WalkDir;
        let max_depth = match is_recursive {
            true => std::usize::MAX,
            false => 1,
        };
        let mut files_found = Vec::new();
        //Get files for every path
        root_paths.iter().for_each(|root_path| {
            let walk_dir = WalkDir::new(root_path)
                .max_depth(max_depth)
                .into_iter()
                .filter_map(|e| e.ok());

            for entry in walk_dir {
                let f_name = entry.file_name().to_string_lossy();
                //If file_extensions add only the matches if not add all files
                match file_extensions {
                    Some(extensions) => {
                        for ext in extensions {
                            if f_name.ends_with(ext) {
                                files_found.push(entry.path().display().to_string());
                            }
                        }
                    }
                    None => panic!("No extensions specified for file search!"),
                }
            }
        });

        files_found
    }

    #[cfg(test)]
    mod tests {
        use crate::file_handler::{get_file_content, get_files_by_extensions};

        #[test]
        fn get_files_by_extensions_recursive_false_test() {
            let extensions = Option::Some(vec![String::from("txt"), String::from("json")]);
            let result = get_files_by_extensions(
                &vec![String::from("./tests/resources")],
                &extensions,
                false,
            );
            assert_eq!(1, result.len());
            assert_eq!(
                vec!["./tests/resources\\test_text_parent_dir.txt".to_string()],
                result
            );
        }

        #[test]
        fn get_files_by_extensions_recursive_true_test() {
            let extensions = Option::Some(vec![String::from("txt"), String::from("json")]);
            let result = get_files_by_extensions(
                &vec![String::from("./tests/resources")],
                &extensions,
                true,
            );
            assert_eq!(7, result.len());
            assert_eq!(
                vec![
                    String::from(
                        "./tests/resources\\sub-folder1\\sub-sub-folder1\\text-file-nested1.txt"
                    ),
                    String::from("./tests/resources\\sub-folder1\\test-empty-file1.txt"),
                    String::from("./tests/resources\\sub-folder1\\test-text1.json"),
                    String::from(
                        "./tests/resources\\sub-folder2\\sub-sub-folder2\\text-file-nested2.txt"
                    ),
                    String::from("./tests/resources\\sub-folder2\\test-empty-file2.txt"),
                    String::from("./tests/resources\\sub-folder2\\test-text2.json"),
                    String::from("./tests/resources\\test_text_parent_dir.txt"),
                ],
                result
            );
        }

        #[test]
        fn get_file_has_content_test() {
            let result =
                get_file_content(&String::from("./tests/resources/test_text_parent_dir.txt"));
            assert!(!result.is_empty());
        }

        #[test]
        fn get_file_content_test() {
            let result =
                get_file_content(&String::from("./tests/resources/test_text_parent_dir.txt"));
            assert_eq!("This is test data for Rust test\r\nThis is line 2 of the test data dummy info here 123 here we go. bar", result);
        }

        #[test]
        fn get_external_file_content_test() {
            let result =
                get_file_content(&String::from("./tests/resources/test_text_parent_dir.txt"));
            assert_eq!("This is test data for Rust test\r\nThis is line 2 of the test data dummy info here 123 here we go. bar", result);
        }

        #[test]
        #[should_panic(
            expected = "Problem opening the file: fake_non_existent_file.txt Error: Os { code: 2, kind: NotFound, message: \"The system cannot find the file specified.\" }"
        )]
        fn get_file_content_invalid_file_test() {
            get_file_content(&String::from("fake_non_existent_file.txt"));
        }
    }
}
